import random
from math import log
from trial_divisor_func import trial_divisor_f

#todo: дописать и отладить тест Миллера-Рабина

def gcd(a,b):#a,b ненулевые
	i=0
	while a%2==0: #представить a в виде 2^i*a1, положить a=a1
		a=a>>1
		i=i+1
	j=0
	while b%2==0: #аналогично для b
		b=b>>1
		j=j+1
	k=min(i,j)
	while a!=b: #выполнять пока а не равно b
		if a<b: #если a<b, поменять а и b местами
			temp=a
			a=b
			b=temp
		c=a-b
		while c%2==0: #представить с в виде 2^s*c1
			c=c>>1
		a=c #положить a=c1 (сразу можно поменять c, так как его значение дальше не нужно)

	return (2**k)*a

def Miller_Rabin_test(n): #алгоритм проверки числа на простоту Миллера-Рабина
	t=min(n-2,5) #параметр надёжности
	s=0
	r=n-1
	while r%2==0:
		s=s+1
		r=r/2
	
	for i in range(t):
		b=random.randint(2,n-2)
		y=(b**r)%n
		if (y!=1) and (y!=(n-1)):
			for j in range(1,s):
				y=(y*y)%n
				if y==1: #если у=1, ответ - "составное"
					return 0
				if y==(n-1):
					break
			if y!=(n-1): #если после последней итерации цикла y не равен -1(mod n), то ответ "составное"
				return 0
	return 1 #вероятно, простое

#начало алгоритма

n=int(input("Введите n: "))
if n%2==0:
	print("n должно быть нечётным")
elif n<0:
	print("n должно быть неотрицательным")
elif Miller_Rabin_test(n)==1:
	print("n должно быть составным")
else:
	B=0
	while B<2:
		B=int(input("Введите границу B>=2: "))
	q=[]
	for i in range(2,B+1):
		if trial_divisor_f(i)==([i],[1]): #ищем простые числа от 2 до B
			q.append(i)
	a=random.randint(2,n-2)
	d=gcd(a,n)
	if d>1:
		print("Ответ: "+str(d))

	else:	
		for q_i in q:
			e_i=int(log(n)/log(q_i))
			a=(a**(q_i**e_i))%n
			if a==1:
				print("Отказ")
				break
			d=gcd(a-1,n)
			if d>1:
				print("Ответ: "+str(d))
				break
		print("Если нет ответов, граница мала")

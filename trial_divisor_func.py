#здесь метод пробных делителей будет переписан под функцию, которая возвращает делители и степени, чтобы в дальнейшем использовать её в подсчёте символа Лежандра

def trial_divisor_f(n):
	divisors = []
	powers=[] #списки p и l (делителей и степеней)

	if n<=0:
		divisors.append(-1)
		powers.append(-1)

	else:
		trial_divisors = [2,3,5,7] 

		k=0
		current_divisor=trial_divisors[k]
		last_divisor=5 #когда закончатся пробные делители из массива, будем генерировать с помощью last_divisor

		while(n>1):
			q=n//current_divisor
			r=n%current_divisor
			if r==0:
				divisors.append(current_divisor)
				n=q
				powers.append(1)
				while n%current_divisor==0:
					powers[-1]=powers[-1]+1
					n=n//current_divisor
			else:
				if current_divisor<q:
					k=k+1
					if k<len(trial_divisors):
						current_divisor=trial_divisors[k]
					else:
						temp=current_divisor #записываем текущий делитель и изменяем его
						current_divisor=last_divisor+6
						last_divisor=temp
				else:
					divisors.append(n)
					powers.append(1)
					break
	return divisors,powers

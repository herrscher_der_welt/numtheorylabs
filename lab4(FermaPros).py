from trial_divisor_func import trial_divisor_f

def Jacobi(a,b):
    a%=b
    r=1
    while a!=0:
        while (a%2)!=1: #избавляемся от чётности a
            a/=2
            if b%8==3 or b%8==5: #если кол-во делений получилось нечётным, то знак в итоге поменяется
                r=-r
        a,b=b,a
        if a%4==b%4==3: #квадратичный закон взаимности
            r=-r
        a%=b
    if b==1:
        return r
    else:
        return 0
	

n=int(input("Enter n:"))
if n<0:
	print("n should be positive")

else:
	while n%2==0:
		n=n/2

	x=int(pow(n,1/2))
	if pow(x,2)==n:
		a=x
		b=x
		print("a=b="+str(x))

	else:
		while True:
			modules_str=input("Введите модули через пробел: ") #принимаем модули от пользователя
			if modules_str.find('-')>-1: #если где-то был введён минус, отправляем вводить ещё раз
				print("Модули должны быть положительные")
			else:
				break
		modules=modules_str.split(' ')
		modules=list(set(modules)) #убираем дупликаты
		for i in range(len(modules)):
			modules[i]=int(modules[i]) #преобразуем строковые значения в целые

		
		sieve=[] #здесь будет таблица просеивания
		for m in modules:
			m_list=[]
			for j in range(m):
				if (j**2-n)%m==0: #отдельно обрабатываем случай, когда j**2=0 mod m[i]
					m_list.append(1)
				elif Jacobi(j**2-n,m)==-1: #если невычет - записываем 0, иначе 1
					m_list.append(0)
				else:
					m_list.append(1)
			sieve.append(m_list)

		x=x+1
		r=[]
		for m in modules:
			r.append(x%m)

		border=(n+9)/6
		while True:
			check_x=1 #флаг, проверяем ли значение x
			for i in range(len(modules)):
				if sieve[i][r[i]]==0: #если в столбце какого-то модуля 0, то не проверяем
					check_x=0
					break
			if(check_x):
				z=x**2-n
				y=int(pow(z,1/2))
				if y**2==z:
					a=x+y
					b=x-y
					print("a="+str(a)+", b="+str(b))
					break
			x=x+1
			if x>border:
				print("число "+str(int(n))+" - простое")
				break
			else:
				for i in range(len(modules)):
					r[i]=(r[i]+1)%modules[i]



#метод пробного деления


n = int(input("Введите число n: "))
B = int(input("Введите границу B либо '-1':"))

if n<=0:
	print('n должно быть положительным')

elif B==0 or B==1:
	print('B должна быть больше 1 либо равна -1')

else:
	divisors = []
	powers=[] #списки p и l (делителей и степеней)
	trial_divisors = [2,3,5,7] 

	k=0
	t=-1
	current_divisor=trial_divisors[k]
	last_divisor=5 #когда закончатся пробные делители из массива, будем генерировать с помощью last_divisor

	while(n>1):
		q=n//current_divisor
		r=n%current_divisor
		if r==0:
			t=t+1
			divisors.append(current_divisor)
			n=q
		else:
			if current_divisor<q:
				k=k+1
				if k<len(trial_divisors):
					current_divisor=trial_divisors[k]
				else:
					temp=current_divisor #записываем текущий делитель и изменяем его
					current_divisor=last_divisor+6
					if B>1 and current_divisor>B:
						print("Факторизация неполная: достигнута граница B="+str(B))
						break
					last_divisor=temp
			else:
				divisors.append(n)
				break

	if t==-1:
		divisors.append(1)

	print("Делители этого числа:")
	print(divisors)

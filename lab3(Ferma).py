def Ferma():
	n=int(input("Enter n:"))
	if n<0:
		print("n should be positive")
		return

	while n%2==0:
		n=n/2

	x=int(pow(n,1/2))
	if pow(x,2)==n:
		a=x
		b=x
		print("a=b="+str(x))
		return

	while True:
		x=x+1
		if x>((n+9)/6):
			print("n is prime")
			return
		z=pow(x,2)-n
		y=int(pow(z,1/2))
		if pow(y,2)==z:
			a=x+y
			b=x-y
			break

	print('a='+str(a))
	print('b='+str(b))


Ferma()

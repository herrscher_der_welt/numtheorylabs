import random
import numpy as np
import math
from trial_divisor_func import trial_divisor_f

def Miller_Rabin_test(n): #алгоритм проверки числа на простоту Миллера-Рабина
	t=min(n-2,5) #параметр надёжности
	s=0
	r=n-1
	while r%2==0:
		s=s+1
		r=r/2
	
	for i in range(t):
		b=random.randint(2,n-2)
		y=(b**r)%n
		if (y!=1) and (y!=(n-1)):
			for j in range(1,s):
				y=(y*y)%n
				if y==1: #если у=1, ответ - "составное"
					return 0
				if y==(n-1):
					break
			if y!=(n-1): #если после последней итерации цикла y не равен -1(mod n), то ответ "составное"
				return 0
	return 1 #вероятно, простое

def gcd(a,b):#a,b ненулевые
	i=0
	while a%2==0: #представить a в виде 2^i*a1, положить a=a1
		a=a>>1
		i=i+1
	j=0
	while b%2==0: #аналогично для b
		b=b>>1
		j=j+1
	k=min(i,j)
	while a!=b: #выполнять пока а не равно b
		if a<b: #если a<b, поменять а и b местами
			temp=a
			a=b
			b=temp
		c=a-b
		while c%2==0: #представить с в виде 2^s*c1
			c=c>>1
		a=c #положить a=c1 (сразу можно поменять c, так как его значение дальше не нужно)

	return (2**k)*a

def isPower(n):
	for k in range(2,math.floor(math.log2(n))):
		if math.modf(n ** (1/k))[0] == 0:
			return True
	return False

n=int(input("Введите n:"))
if n%2==0:
	print("n должно быть нечётным") #проверить, что не степень
elif Miller_Rabin_test(n)==1:
	print("n должно быть составным")
elif isPower(n)==True:
	print("n не должно быть степенью")

else:
	k=int(input("Введите размер базы (k): "))
	S=[2,3,5,7]
	if k<1:
		print("k должно быть больше нуля")
	else:
		if k<4: #если k меньше 4, то оставляем в базе k элементов
			S=S[:k]
		else:
			for i in range(4,k): #иначе генерируем факторную базу большей длины, добавляя к предпоследнему элементу 6
				S.append(S[i-2]+6)

		t=k+1
		V=[]
		a_i=[]
		while len(V)<t:
			a=random.randint(1,n)
			b=(a**2)%n
			b_divisors,b_powers=trial_divisor_f(b)
			if b_divisors[-1]<=S[-1]: #если максимальный простой делитель не превышает максимального делителя базы, записываем степени
				powers=[]
				for s in S:			
					i=0
					while i<len(b_divisors) and s!=b_divisors[i]:
						i+=1
					if i==len(b_divisors): #если такого делителя в b нет, пишем степень 0
						powers.append(0)
					else: #иначе пишем степень
						powers.append(b_powers[i]%2)
				V.append(powers) 
				a_i.append(a) #записываем первый элемент пары

		for l in range(1,2**t):
			#перебираем возможные значения векторов, чтобы точно перебрать все решения
			c=list(bin(l)[2:]) #двоичное значение счётчика l строкой
			while len(c)<t:
				c.insert(0,'0') #если надо, дописываем ведущие нули
			for j in range(t):
				c[j]=int(c[j])
			if(np.count_nonzero(np.dot(c,V)%2)==0): #если решение подошло
				x=1
				y=1
				for i in range(t):
					if c[i]==1:
						x=(x*a_i[i])%n
				for j in range(k):
					power=0
					for i in range(t):
						if c[i]==1:
							power+=V[i][j]
					power//=2 #степень= сумма e_ij / 2
					y=(y*(S[j]**power))%n
				if x!=y and x!=(-y): #если нашлись делители, выводим их и выходим из цикла. Иначе берём следущее решение
					print("Делители: "+str(gcd(x-y,n))+", "+str(gcd(x+y,n)))
					break

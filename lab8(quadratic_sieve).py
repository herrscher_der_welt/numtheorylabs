import random
import numpy as np
import math
from trial_divisor_func import trial_divisor_f

def Miller_Rabin_test(n): #алгоритм проверки числа на простоту Миллера-Рабина
	t=min(n-2,5) #параметр надёжности
	s=0
	r=n-1
	while r%2==0:
		s=s+1
		r=r/2
	
	for i in range(t):
		b=random.randint(2,n-2)
		y=(b**r)%n
		if (y!=1) and (y!=(n-1)):
			for j in range(1,s):
				y=(y*y)%n
				if y==1: #если у=1, ответ - "составное"
					return 0
				if y==(n-1):
					break
			if y!=(n-1): #если после последней итерации цикла y не равен -1(mod n), то ответ "составное"
				return 0
	return 1 #вероятно, простое

def gcd(a,b):#a,b ненулевые
	i=0
	while a%2==0: #представить a в виде 2^i*a1, положить a=a1
		a=a>>1
		i=i+1
	j=0
	while b%2==0: #аналогично для b
		b=b>>1
		j=j+1
	k=min(i,j)
	while a!=b: #выполнять пока а не равно b
		if a<b: #если a<b, поменять а и b местами
			temp=a
			a=b
			b=temp
		c=a-b
		while c%2==0: #представить с в виде 2^s*c1
			c=c>>1
		a=c #положить a=c1 (сразу можно поменять c, так как его значение дальше не нужно)
	return (2**k)*a

def L(a,p):
	if a==-1:
		return (-1)**((p-1)/2)
	if a==0:
		return 0
	if a<0:
		a=-a
		return L(a,p)*L(-1,p)
	a=a%p
	if a==1: #проверяем простые случаи
		return 1
	a_divisors,alpha=trial_divisor_f(a) #раскладываем а на простые множители
	if len(a_divisors)==1: #если число уже простое
		if a==2:
			return (-1)**((p**2-1)/8) #считаем символ Лежандра для a=2
		else: #иначе применяем закон взаимности
			return ((-1)**( ((p-1)/2) * ((a-1)/2) )) * L(p,a)
	else: #если число не простое
		answer=1
		for i in range(len(a_divisors)):
			if alpha[i]%2==1:
				answer=answer*L(a_divisors[i],p)
		return answer

def isPower(n):
	for k in range(2,math.floor(math.log2(n))):
		if math.modf(n**(1/k))[0]==0:
			return True
	return False


n=int(input("Введите n:"))
if n%2==0:
	print("n должно быть нечётным")
elif Miller_Rabin_test(n)==1:
	print("n должно быть составным")
elif isPower(n)==True:
	print("n не должно быть степенью")

else:
	k=int(input("Введите размер базы (k): "))
	if k<3:
		print("k должно быть 3 и больше")
	else:
		primes=[2,3,5,7]
		last_but_one_prime=5
		last_prime=7 
		S=[-1,2]
		primes_index=1
		while len(S)<k: #генерируем простые числа, если символ Лежандра равен 1, то добавляем число в базу
			if n%primes[primes_index]!=0:
				if L(n,primes[primes_index])==1:
					S.append(primes[primes_index])
			primes_index+=1
			if primes_index==len(primes):
				next_prime=primes[primes_index-2]+6 #правильно генерирует первые 9 простых чисел
				divisors,powers=trial_divisor_f(next_prime)
				while len(divisors)>1:
					foo=last_but_one_prime+6
					last_but_one_prime=last_prime
					last_prime=next_prime
					next_prime=foo
					divisors,powers=trial_divisor_f(next_prime)
				primes.append(next_prime) 

		m=int(n**(1/2))
		a_arr=[]
		V=[]
		t=k+1
		x=0
		while len(V)<t:
			a=x+m
			b=a**2-n
			b_divisors,b_powers=trial_divisor_f(abs(b))
			if b<0:
				b_divisors.insert(0,-1) #добавляем в список делителей -1 при необходимости
				b_powers.insert(0,1)
			not_base_divisors=list(set(b_divisors)-set(S)) #список делителей b, которых нет в базе
			if len(not_base_divisors)>0: #если нет разложения
				if x==(-x): #если x равен 0, берём следующий, иначе сначала пробуем -x
					x+=1
					continue
				else:
					a=m-x
					b=a**2-n
					b_divisors,b_powers=trial_divisor_f(b)
					not_base_divisors=list(set(b_divisors)-set(S)) #список делителей b, которых нет в базе
					if len(not_base_divisors)>0:
						x+=1
						continue
			#если не перешли на следующий шаг цикла, значит, разложение есть
			a_arr.append(a)
			powers=[]
			for s in S:			
				i=0
				while i<len(b_divisors) and s!=b_divisors[i]:
					i+=1
				if i==len(b_divisors): #если такого делителя в b нет, пишем степень 0
					powers.append(0)
				else: #иначе пишем степень
					powers.append(b_powers[i]%2)
			V.append(powers) 
			x+=1
		#дальше как в Диксоне
		for l in range(1,2**t):
			#перебираем возможные значения векторов, чтобы точно перебрать все решения
			c=list(bin(l)[2:]) #двоичное значение счётчика l строкой
			while len(c)<t:
				c.insert(0,'0') #если надо, дописываем ведущие нули
			for j in range(t):
				c[j]=int(c[j])
			if(np.count_nonzero(np.dot(c,V)%2)==0): #если решение подошло
				x=1
				y=1
				for i in range(t):
					if c[i]==1:
						x=(x*a_arr[i])%n
				for j in range(k):
					power=0
					for i in range(t):
						if c[i]==1:
							power+=V[i][j]
					power//=2 #степень= сумма e_ij / 2
					y=(y*(S[j]**power))%n
				if x!=y and x!=(-y): #если нашлись делители, выводим их и выходим из цикла. Иначе берём следущее решение
					first=gcd(abs(x-y),n)
					second=gcd(x+y,n)
					if first==1 and second==1:
						continue
					print("Делители: "+str(first)+", "+str(second))
					break
		print("Если не нашлось делителей, увеличьте размер базы")

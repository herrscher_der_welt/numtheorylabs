def gcd(a,b):#a,b ненулевые
	i=0
	while a%2==0: #представить a в виде 2^i*a1, положить a=a1
		a=a>>1
		i=i+1
	j=0
	while b%2==0: #аналогично для b
		b=b>>1
		j=j+1
	k=min(i,j)
	while a!=b: #выполнять пока а не равно b
		if a<b: #если a<b, поменять а и b местами
			temp=a
			a=b
			b=temp
		c=a-b
		while c%2==0: #представить с в виде 2^s*c1
			c=c>>1
		a=c #положить a=c1 (сразу можно поменять c, так как его значение дальше не нужно)

	return (2**k)*a

def f(x):
	return (x**2+1)

#начало алгоритма ро-метода Полларда

n=int(input("Введите n: "))
if n%2==0 or n<=0:
	print('n должно быть положительным и нечетным')

else:
	d=1
	a=2
	b=2
	while d==1:
		a=f(a)%n
		b=f(f(b))%n
		if a==b:
			print("Отказ")
			break
		else:
			d=gcd(abs(a-b),n)

	if d!=1: #если выход произошёл не аварийно
		print("Ответ: "+str(d))
